<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentHistory extends Model
{
    protected $fillable = [
        'department_id','status_id','updated_by','remarks','is_current'
    ];
}
