<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeHistory extends Model
{
    protected $fillable = [
        'company_id','status_id','updated_by','remarks','is_current'
    ];
}
