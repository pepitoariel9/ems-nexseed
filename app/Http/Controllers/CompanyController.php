<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\Company\StoreRequest;
use App\Http\Requests\Company\UpdateRequest;
use App\Mail\NewCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    protected $page = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::query()->orderByDesc('created_at')
            ->with('departments')
            ->paginate($this->page);

        return view('pages.company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $company = Company::create(array_merge($request->validated(), ['status_id' => 1]));
            $company->createHistory('added');

            Mail::to(auth()->user()->email)->queue(
                new NewCompany([
                    'company_name' => $company->name,
                    'created_by'   => auth()->user()->name,
                ])
            );
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('companies.index')->with('error', 'Error storing company details');
        }

        DB::commit();

        return redirect()->route('companies.index')->with('success', 'Company is added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('pages.company.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('pages.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Company $company)
    {
        DB::beginTransaction();
        try {
            $company->fill($request->validated());
            $company->save();

            $company->createHistory('updated');

            $company->departments()->sync($request->department_ids);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('companies.index')->with('error', 'Error updating company details');
        }

        DB::commit();

        return redirect()->route('companies.index')->with('success', 'Company is updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        DB::beginTransaction();
        try {
            $company->delete();

            $company->createHistory('deleted');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('companies.index')->with('error', 'Error deleting company');
        }

        DB::commit();

        return redirect()->route('companies.index')->with('success', 'Company is deleted successfully');
    }
}
