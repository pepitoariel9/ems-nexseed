<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests\Department\StoreRequest;
use App\Http\Requests\Department\UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentController extends Controller
{
    protected $page = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::query()->orderByDesc('created_at')
            ->with('employees')
            ->paginate($this->page);

        return view('pages.department.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $department = Department::create(array_merge($request->validated(), ['status_id' => 1]));
            
            $department->createHistory('added');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('departments.index')->with('error', 'Error storing department details');
        }

        DB::commit();

        return redirect()->route('departments.index')->with('success', 'Department is added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        return view('pages.department.show', compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('pages.department.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Department $department)
    {
        DB::beginTransaction();
        try {
            $department->fill($request->validated());
            $department->save();

            $department->createHistory('updated');

            $department->employees()->sync($request->employee_ids);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('departments.index')->with('error', 'Error updating department details');
        }

        DB::commit();

        return redirect()->route('departments.index')->with('success', 'Department is updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        DB::beginTransaction();
        try {
            $department->delete();

            $department->createHistory('deleted');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('departments.index')->with('error', 'Error deleting department');
        }

        DB::commit();

        return redirect()->route('departments.index')->with('success', 'Department is deleted successfully');
    }
}
