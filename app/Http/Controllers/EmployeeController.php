<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\Employee\StoreRequest;
use App\Http\Requests\Employee\UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    protected $page = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::query()->orderByDesc('created_at')
            ->with('departments')
            ->paginate($this->page);

        return view('pages.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $employee = Employee::create(array_merge($request->validated(), ['status_id' => 1]));
            
            $employee->createHistory('added');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('employees.index')->with('error', 'Error storing employee details');
        }

        DB::commit();

        return redirect()->route('employees.index')->with('success', 'Employee is added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('pages.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('pages.employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Employee $employee)
    {
        DB::beginTransaction();
        try {
            $employee->fill($request->validated());
            $employee->save();

            $employee->createHistory('updated');

            $employee->employees()->sync($request->department_ids);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('employees.index')->with('error', 'Error updating employee details');
        }

        DB::commit();

        return redirect()->route('employees.index')->with('success', 'Employee is updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        DB::beginTransaction();
        try {
            $employee->delete();

            $employee->createHistory('deleted');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('employees.index')->with('error', 'Error deleting employee');
        }

        DB::commit();

        return redirect()->route('employees.index')->with('success', 'Employee is deleted successfully');
    }
}
