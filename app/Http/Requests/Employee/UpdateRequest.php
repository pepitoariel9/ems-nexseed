<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'position' => 'required|max:255',
            'contact_number' => 'required|max:255',
            'email' => 'required|email',
            'full_address' => 'required|max:255',
            'status_id' => 'required|exists:employee_statuses,id'
        ];
    }

    public function attributes()
    {
        return [
            'full_address' => 'address'
        ];
    }
}
