<?php

namespace App\Http\Requests\Company;

use App\Department;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'           => 'required|max:255',
            'description'    => 'required|max:255',
            'status_id'      => 'required|exists:company_statuses,id',
        ];

        $departmentIds = [];

        if ($this->department_ids) {
            $departmentIds = Department::whereIn($this->department_ids)->pluck('id')->toArray();
            $rules['department_ids'] = ['nullable','in:' . join(',', $departmentIds)];
        }

        return $rules;
    }
}
