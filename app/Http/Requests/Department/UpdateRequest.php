<?php

namespace App\Http\Requests\Department;

use App\Employee;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'           => 'required|max:255',
            'description'    => 'required|max:255',
            'status_id'      => 'required|exists:department_statuses,id',
        ];

        if ($this->employee_ids) {
            $employeeIds = Employee::whereIn($this->employee_ids)->pluck('id')->toArray();
            $rules['employee_ids'] = ['nullable','in:' . join(',', $employeeIds)];
        }

        return $rules;
    }
}
