<?php

namespace App;

use App\Department;
use App\EmployeeHistory;
use App\EmployeeStatus;
use App\Traits\HasHistory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes, HasHistory;

    protected $fillable = [
        'first_name','last_name','position','contact_number','email','full_address','status_id'
    ];


    public function status()
    {
        return $this->belongsTo(EmployeeStatus::class, 'status_id');
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'department_employees');
    }

    public function histories()
    {
        return $this->hasMany(EmployeeHistory::class, 'employee_id');
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
