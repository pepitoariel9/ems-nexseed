<?php

namespace App;

use App\CompanyDepartment;
use App\CompanyHistory;
use App\Department;
use App\Traits\HasHistory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes, HasHistory;

    protected $fillable = [
        'name', 'description','status_id'
    ];


    public function status()
    {
        return $this->belongsTo(CompanyStatus::class, 'status_id');
    }


    public function histories()
    {
        return $this->hasMany(CompanyHistory::class, 'company_id');
    }

    public function companyDepartments()
    {
        return $this->hasMany(CompanyDepartment::class, 'company_id');
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'company_departments');
    }
}
