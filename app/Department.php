<?php

namespace App;

use App\Company;
use App\DepartmentHistory;
use App\DepartmentStatus;
use App\Employee;
use App\Traits\HasHistory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes, HasHistory;

    protected $fillable = [
        'name', 'description','status_id'
    ];


    public function status()
    {
        return $this->belongsTo(DepartmentStatus::class, 'status_id');
    }


    public function histories()
    {
        return $this->hasMany(DepartmentHistory::class, 'department_id');
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_departments');
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'department_employees');
    }
}
