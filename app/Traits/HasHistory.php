<?php

namespace App\Traits;

trait HasHistory
{
    public function createHistory($remarks = null)
    {
        $this->histories()->where('is_current', 1)->update(['is_current' => 0]);

        $this->histories()->create([
            'status_id'  => $this->status_id,
            'remarks'    => $remarks,
            'is_current' => 1,
            'updated_by' => auth()->user()->id
        ]);
    }
}
