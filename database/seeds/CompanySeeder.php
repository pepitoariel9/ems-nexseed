<?php

use App\CompanyStatus;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Active',
            ],
            [
                'name' => 'Inactive',
            ]
        ];

        foreach ($data as $d) {
            CompanyStatus::create($d);
        }
    }
}
