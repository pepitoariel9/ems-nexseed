<?php

use App\EmployeeStatus;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Active',
            ],
            [
                'name' => 'Inactive',
            ],
            [
                'name' => 'Terminated',
            ],
            [
                'name' => 'Suspended',
            ]
        ];

        foreach ($data as $d) {
            EmployeeStatus::create($d);
        }
    }
}
