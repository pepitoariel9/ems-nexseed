<?php

use App\DepartmentStatus;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Active',
            ],
            [
                'name' => 'Inactive',
            ]
        ];

        foreach ($data as $d) {
            DepartmentStatus::create($d);
        }
    }
}
