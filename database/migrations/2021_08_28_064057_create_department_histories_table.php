<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('updated_by');
            $table->string('remarks')->nullable();
            $table->boolean('is_current');
            $table->timestamps();

            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('status_id')->references('id')->on('department_statuses');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_histories');
    }
}
