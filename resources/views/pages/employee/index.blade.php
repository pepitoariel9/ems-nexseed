@extends('adminlte::page')

@section('title', 'Employees')

@section('content_header')
    <h1 class="m-0 text-dark">Employees</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Employees List</h3>
                    <div class="card-tools">
                        <a href="{{ route('employees.create') }}" class="btn btn-primary">Create</a>
                    </div>
                  </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($employees as $employee)
                                    <tr>
                                        <td>{{ $employee->full_name }}</td>
                                        <td>{{ $employee->position }}</td>
                                        <td>{{ $employee->email }}</td>
                                        <td>{{ $employee->contact_number }}</td>
                                        <td>{{ $employee->address }}</td>
                                        <td>
                                            <a href="{{ route('employees.edit',$employee->id) }}" class="btn btn-info">Edit</a>
                                            <a href="{{ route('employees.show',$employee->id) }}" class="btn btn-info">Show</a>
                                            
                                            <form  action="{{ route('employees.destroy',$employee->id) }}" class="submit-delete-form" method="POST" style="display: inline;"> @csrf @method('delete')
                                                <a href="#" class="btn btn-danger delete">Delete</a>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6" class="text-center">No records</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $employees->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
<script type="text/javascript">

    $('table tbody').on('click','.delete',function(e){
        e.preventDefault();
        var url = $(this).data('route');
        var that = this;

        Swal.fire({
            icon: 'info',
            title: 'Delete Department',
            text: 'Do you really want to delete?',
            showCancelButton: true,
            showConfirmButton:true,
            preConfirm: (res) => {
                that.closest('form').submit();
            }
        })
    });
    
    
</script>
@endpush
