@extends('adminlte::page')

@section('title', 'Edit Employee')

@section('content_header')
    <h1 class="m-0 text-dark">Employees</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit Employee</h3>
                  </div>
                <div class="card-body">
                    <form action="{{ route('employees.update', $employee->id) }}" method="post">
                        @csrf
                        @method('put')
                        <x-adminlte-input name="first_name" label="First name" placeholder="First name"
                                fgroup-class="col-md-12" value={{ $employee->firt_name}}/>

                        <x-adminlte-input name="last_name" label="Last name" placeholder="Last name"
                                fgroup-class="col-md-12" value={{ $employee->last_name}}/>

                        <x-adminlte-input name="contact_number" label="Contact number" placeholder="Contact number"
                                fgroup-class="col-md-12" value={{ $employee->contact_number}}/>

                        <x-adminlte-input name="position" label="Position" placeholder="Position"
                                fgroup-class="col-md-12" value={{ $employee->position}}/>

                        <x-adminlte-input name="email" label="Email" placeholder="Email"
                                fgroup-class="col-md-12" value={{ $employee->email}}/>

                        <x-adminlte-input name="full_address" label="Address" placeholder="address"
                                fgroup-class="col-md-12" value={{ $employee->full_address}}/>


                        <x-adminlte-button  type="submit" label="submit" class="ml-2" theme="primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
