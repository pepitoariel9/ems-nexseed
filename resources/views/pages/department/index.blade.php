@extends('adminlte::page')

@section('title', 'Departments')

@section('content_header')
    <h1 class="m-0 text-dark">Departments</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Departments List</h3>
                    <div class="card-tools">
                        <a href="{{ route('departments.create') }}" class="btn btn-primary">Create</a>
                    </div>
                  </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($departments as $department)
                                    <tr>
                                        <td>{{ $department->name }}</td>
                                        <td>{{ $department->description }}</td>
                                        <td>{{ $department->status->name }}</td>
                                        <td>
                                            <a href="{{ route('departments.edit',$department->id) }}" class="btn btn-info">Edit</a>
                                            <a href="{{ route('departments.show',$department->id) }}" class="btn btn-info">Show</a>
                                            
                                            <form  action="{{ route('departments.destroy',$department->id) }}" class="submit-delete-form" method="POST" style="display: inline;"> @csrf @method('delete')
                                                <a href="#" class="btn btn-danger delete">Delete</a>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">No records</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $departments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
<script type="text/javascript">

    $('table tbody').on('click','.delete',function(e){
        e.preventDefault();
        var url = $(this).data('route');
        var that = this;

        Swal.fire({
            icon: 'info',
            title: 'Delete Department',
            text: 'Do you really want to delete?',
            showCancelButton: true,
            showConfirmButton:true,
            preConfirm: (res) => {
                that.closest('form').submit();
            }
        })
    });
    
    
</script>
@endpush
