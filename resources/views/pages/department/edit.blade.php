@extends('adminlte::page')

@section('title', 'Edit Department')

@section('content_header')
    <h1 class="m-0 text-dark">Departments</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit Department - {{ $department->name }}</h3>
                  </div>
                <div class="card-body">
                    <form action="{{ route('departments.update',$department->id) }}" method="post">
                        @csrf
                        @method('put')
                        <x-adminlte-input name="name" label="Name" placeholder="name"
                                fgroup-class="col-md-12" value="{{ $department->name }}"/>

                        <x-adminlte-textarea name="description" fgroup-class="col-md-12"  label="Description" placeholder="description">{{ $department->description }}</x-adminlte-textarea>

                        <x-adminlte-select name="status_id" fgroup-class="col-md-12" label="Status">
                            <x-adminlte-options :options="[1 => 'Active', 0 => 'Inactive']" selected="{{ $department->status_id}}"
                                empty-option="Select one"/>
                        </x-adminlte-select>

                        <x-adminlte-button  type="submit" label="submit" class="ml-2" theme="primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
