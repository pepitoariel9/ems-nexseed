@extends('adminlte::page')

@section('title', 'Companies')

@section('content_header')
    <h1 class="m-0 text-dark">Companies</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Companies List</h3>
                    <div class="card-tools">
                        <a href="{{ route('companies.create') }}" class="btn btn-primary">Create</a>
                    </div>
                  </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($companies as $company)
                                    <tr>
                                        <td>{{ $company->name }}</td>
                                        <td>{{ $company->description }}</td>
                                        <td>{{ $company->status->name }}</td>
                                        <td>
                                            <a href="{{ route('companies.edit',$company->id) }}" class="btn btn-info">Edit</a>
                                            <a href="{{ route('companies.show',$company->id) }}" class="btn btn-info">Show</a>
                                            
                                            <form  action="{{ route('companies.destroy',$company->id) }}" class="submit-delete-form" method="POST" style="display: inline;"> @csrf @method('delete')
                                                <a href="#" class="btn btn-danger delete">Delete</a>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">No records</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $companies->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
<script type="text/javascript">

    $('table tbody').on('click','.delete',function(e){
        e.preventDefault();
        var url = $(this).data('route');
        var that = this;

        Swal.fire({
            icon: 'info',
            title: 'Delete Company',
            text: 'Do you really want to delete?',
            showCancelButton: true,
            showConfirmButton:true,
            preConfirm: (res) => {
                that.closest('form').submit();
            }
        })
    });
    
    
</script>
@endpush
