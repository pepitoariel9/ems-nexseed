@extends('adminlte::page')

@section('title', 'New Company')

@section('content_header')
    <h1 class="m-0 text-dark">Companies</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">New Company</h3>
                  </div>
                <div class="card-body">
                    <form action="{{ route('companies.store') }}" method="post">
                        @csrf
                        @method('post')
                        <x-adminlte-input name="name" label="Name" placeholder="name"
                                fgroup-class="col-md-12"/>

                        <x-adminlte-textarea name="description" fgroup-class="col-md-12"  label="Description" placeholder="description"/>

                        <x-adminlte-button  type="submit" label="submit" class="ml-2" theme="primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
