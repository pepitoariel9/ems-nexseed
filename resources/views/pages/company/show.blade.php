@extends('adminlte::page')

@section('title', 'Show Company')

@section('content_header')
    <h1 class="m-0 text-dark">Companies</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Company Details - {{ $company->name }}</h3>
                  </div>
                <div class="card-body">
                        <x-adminlte-input name="name" label="Name" placeholder="name"
                                fgroup-class="col-md-12" value="{{ $company->name }}" readonly/>

                        <x-adminlte-textarea name="description" fgroup-class="col-md-12"  label="Description" placeholder="description" readonly>{{ $company->description }}</x-adminlte-textarea>

                        <x-adminlte-select readonly name="status_id" fgroup-class="col-md-12" label="Status">
                            <x-adminlte-options :options="[1 => 'Active', 0 => 'Inactive']" selected="{{ $company->status_id}}"
                                empty-option="Select one"/>
                        </x-adminlte-select>

                        <x-adminlte-input name="name" label="Created At" placeholder="name"
                                fgroup-class="col-md-12" value="{{ $company->created_at }}" readonly/>

                        <a href="{{ route('companies.index')}}" class="btn btn-default ml-2">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
